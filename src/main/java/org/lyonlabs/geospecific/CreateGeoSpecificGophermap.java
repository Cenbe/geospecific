/**************************************************************************
  CreateGeoSpecificGopherMap.java
 **************************************************************************/

package org.lyonlabs.geospecific;

import org.lyonlabs.geogopher.gophermap.*;
import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;
import org.apache.logging.log4j.*;
import org.lyonlabs.d64.utility.*;
import org.lyonlabs.geogopher.gophermap.GopherItem.GopherType;

/**
 * Create gophermaps for the geoSpecific collection.
 */
public class CreateGeoSpecificGophermap {
  private static final Logger logger = LogManager.getLogger(
                       CreateGeoSpecificGophermap.class.getName());
  private final String VERSION = "1.01";
  private final String GOPHERNAME = "gophermap";
  private String host, gopherDir;
  private final int PORT = 6470;
  private enum Machine { greyhand, www, orac, mafarka };
  private static Machine target;
  private final Set<String> allSuffixes;
  
  public CreateGeoSpecificGophermap() {    
    logger.info("---------------------------------");
    logger.info(" CreateGeoSpecificGophermap " + VERSION);
    logger.info("---------------------------------");
    
    switch (target) {
      case greyhand:
        host = "greyhand.lyonlabs.org";
        gopherDir = "/mnt/common/download/c64/os/geos/geoSpecific";
        break;
      case www:
        host = "www.lyonlabs.org";
        gopherDir = "/var/www/html/commodore/onrequest/geos/geoSpecific";
        break;
      case orac:
        host = "orac.lyonlabs.org";
        gopherDir = "/mnt/common/download/c64/os/geos/geoSpecific";
        break;
      case mafarka:
        host = "mafarka.lyonlabs.org";
        gopherDir = "/mnt/common/download/c64/os/geos/geoSpecific";
        break;
      default:
        logger.error("Unknown target: " + target);
    }

    allSuffixes = D64Utility.getImageSuffixes();
    Map<String, String> fileMap = new TreeMap<>();
    
    try (BufferedWriter writer = Files.newBufferedWriter(
                        Paths.get(gopherDir, GOPHERNAME),
                        StandardOpenOption.CREATE,
                        StandardOpenOption.TRUNCATE_EXISTING,
                        StandardOpenOption.WRITE,
                        StandardOpenOption.SYNC)) {
      new GopherItem(GopherType.DIR, "root directory", "/",
                     host, PORT).write(writer);
      new GopherInfoItem(
          "This is most of Bruce Thomas' geoSpecific collection.")
         .write(writer);
      new GopherInfoItem(
          "I haven't included things like .ZIP and .PDF files, since")
         .write(writer);
      new GopherInfoItem(
          "this site is meant mainly for Commodore 64 users. For the entire ")
         .write(writer);
      new GopherInfoItem(
          "collection, look in the GEOS section of my web site:")
         .write(writer);
      new GopherInfoItem(
          "https://www.lyonlabs.org/commodore/")
         .write(writer);
      new GopherItem(GopherType.SEARCH, "Search geoSpecific collection",
                     gopherDir, host, PORT).write(writer);
      
      // addDirectoryItem clears the fileMap
      fileMap.put("BOGEOS1.D64", "geoBEAP, MajorReOrg, geoBeaver");
      fileMap.put("BOGEOS2.D64", "geoBrowser (file and graphics viewer)");
      addDirectoryItem(
            "Bo Zimmerman (GeoBeap, MajorReOrg, geoBeaver, geoBrowser)", 
            gopherDir + "/BoZ/Files", "boz.txt", fileMap, writer);
      
      addDirectoryItem(
            "Bruce Thomas (Articles and Columns)",
            gopherDir + "/BruceT/Files", "brucet.txt", fileMap, writer);
      
      addDirectoryItem(
            "Dale Lutes (Games and Guitar Music fonts)", 
            gopherDir + "/DaleL/Files", "DLutes.txt", fileMap, writer);
      
      addDirectoryItem(
            "Dave Ferguson (Dweezil Disk 1 featuring NewTools2)", 
            gopherDir + "/DaveF/Files", "davef.txt", fileMap, writer);

      new GopherItem(GopherType.TEXT, 
            "Dick Estel (Fonts, Graphics, Utilities, Scanned Art)", 
            gopherDir + "/DickE/Files/DickE.txt", host, PORT).write(writer);
      addDirectoryItem(
            "Dick Estel (Fonts)", 
            gopherDir + "/DickE/Files/Fonts", null, fileMap, writer);
      addDirectoryItem(
            "Dick Estel (Graphics)", 
            gopherDir + "/DickE/Files/Graphics", null, fileMap, writer);
      addDirectoryItem(
            "Dick Estel (Utilities)", 
            gopherDir + "/DickE/Files/Other", null, fileMap, writer);
// PDF files, dont' include
//      addDirectoryItem(
//            "Dick Estel (Fonts, Graphics, Utilities, Scanned Art)", 
//            gopherDir + "/DickE/Files/PDF", null, fileMap, writer);
      addDirectoryItem(
            "Dick Estel (Scanned Art)", 
            gopherDir + "/DickE/Files/Scans", null, fileMap, writer);

      addDirectoryItem(
            "GEOS Games", 
            gopherDir + "/Games/Files", "games.txt", fileMap, writer);
      
      addDirectoryItem(
            "M&T Games",
            gopherDir + "/Games/M&T", "PDREADME.TXT", fileMap, writer);
      
// ZIP files, don't include
//      addDirectoryItem(
//            "GeoDOS V2.95 (Full version plus English Documentation)", 
//            gopherDir + "/GeoDOS295/Files", "geodos.txt", fileMap, writer);
      
      addDirectoryItem(
            "GeoManiacs Collection (organized by Gaelyne Gasson)", 
            gopherDir + "/GeoManiacs/Files", "geoman.txt", fileMap, writer);
      
      createGeoMetrixMap(fileMap);      
      addDirectoryItem(
            "Disk collection from the GeoMetrix GEOS User Group", 
            gopherDir + "/GeoMetrix/Files", "geomet.txt", fileMap, writer);
      
      addDirectoryItem(
            "George Wells (Printer Drivers and an Editor)", 
            gopherDir + "/GeorgeW/Files", "georgew.txt", fileMap, writer);
      
      addDirectoryItem(
            "Newsletters and PD Software from GEOS Technical Users Group", 
            gopherDir + "/GeoTUG/Files", "GeoTUG.txt", fileMap, writer);
      
      addDirectoryItem(
            "GeoVision Collection", 
            gopherDir + "/GeoVision/Files", "geovis.txt", fileMap, writer);
      
      addDirectoryItem(
            "GeoWorld Collection", 
            gopherDir + "/GeoWorld/Files", "geoworld.txt", fileMap, writer);
      
      addDirectoryItem(
            "Jean Major (DBGetFiles, GeoSourcer, utilities)", 
            gopherDir + "/JeanM/Files", "jeanm.txt", fileMap, writer);
      
      addDirectoryItem(
            "Kent Smotherman (FreedomWare disks - Games, GeoCheckBook, etc.)", 
            gopherDir + "/KentS/Files", "kents.txt", fileMap, writer);
      
      addDirectoryItem(
            "Maurice Randall (GeoWriteImport, Demo Files, Finder, Screen2Paint)",
            gopherDir + "/MauriceR/Files", "Maurice.txt", fileMap, writer);
      
      addDirectoryItem(
            "Mystic Jim introductory disks", 
            gopherDir + "/MysticJim/Files", "mysticj.txt", fileMap, writer);
      
      addDirectoryItem(
            "Nate Fiedler", 
            gopherDir + "/NateF/Files", "natef.txt", fileMap, writer);
      addDirectoryItem(
            "Nate Fiedler: GeoCanvas (GEOS versions)",
             gopherDir + "/NateF/Files/ForGEOS", null, fileMap, writer);
      addDirectoryItem(
            "Nate Fiedler: GeoCanvas (Wheels versions)",
             gopherDir + "/NateF/Files/ForWheels", "WCANPAT.SRC", fileMap, writer);
      
      addDirectoryItem(
            "Paul Murdaugh (LandMark Series Disk - Dualtop, Games)", 
            gopherDir + "/PaulM/Files", "paulm.txt", fileMap, writer);
      
      addDirectoryItem(
            "Phil Heberer (Ten 1581 disks full of PD Software)", 
            // NOTE INCONSISTENT PATH (lower-case 'f')
            gopherDir + "/PhilH/files", "philh.txt", fileMap, writer);
      
      addDirectoryItem(
            "Randy Winchester (GeoDisk, Laser Printer Utilities, AntiGrav ToolKit)", 
            gopherDir + "/RandyW/Files", "RandyW.txt", fileMap, writer);
      new GopherInfoItem("HTML versions of Randy's \"Antigrav Toolkit\" "
          + "articles can be read").write(writer); 
      new GopherInfoItem("in the GEOS section of my web site:").write(writer);
      new GopherInfoItem("https://www.lyonlabs.org/commodore/").write(writer);      
      
      addDirectoryItem(
            "Rick Coleman (Mover 3.0 disk - Photo Mover, Text Mover, utilities)", 
            gopherDir + "/RickC/Files", "rickc.txt", fileMap, writer);
      
      addDirectoryItem(
            "Roger Lawhorn (GeoPrint, GeoLabel, SuperBox, GeoSIDPlayer, etc.)", 
            gopherDir + "/RogerL/Files", "rogerl.txt", fileMap, writer);
      
      addDirectoryItem(
            "Spike Dethman (Paint Rotate, ClickPix, GeoPack, etc.)", 
            gopherDir + "/SpikeD/Files", "spiked.txt", fileMap, writer);
      
      addDirectoryItem(
            "Todd Elliott (DotView plus other programs patched for Wheels)", 
            gopherDir + "/ToddE/Files", "todde.txt", fileMap, writer);
      
// CVT files only, don't include      
//      addDirectoryItem(
//            "Tools (Convert, GeoBEAP, GeoConvert98f, GeoPack, GeoZip, Wraptor)", 
//            gopherDir + "/Tools/Files", "tools.txt", fileMap, writer);
      
      addDirectoryItem(
            "Various (Utilities, LW Fonts, Great White GEOS programs, GeoFamily, etc.)", 
            gopherDir + "/Various/Files", "Various.txt", fileMap, writer);

// ZIP files, don't include
//      addDirectoryItem(
//            "Patches for Year 2000 and to run GEOS apps under MegaPatch3", 
//            gopherDir + "/Y2K/Files", null, fileMap, writer);
    } catch (Exception exc) {
      logger.error(exc.getMessage(), exc);
    }
  }

  /**
   * Semi-automated creation of a directory's gophermap with file entries.
   * @param  description The directory item's description.
   * @param  dirName The directory the gophermap is being written for.
   * @param  readme The name of a contents file in the given directory.
   * @param  fileMap A (possibly empty) list of files and their descriptions.
   * @param  writer The Writer that will write the gophermap.
   */
  private void addDirectoryItem(String description, String dirName, 
               String readme, Map<String, String>fileMap, Writer writer) 
               throws IOException {
    BufferedWriter fileWriter = null;
    List<Path> files;
    List<GopherItem> items;
    String selector;
    int parents;
      
    logger.debug("dirName: " + dirName);
    logger.info("writing directory entry for "
              + dirName.substring(gopherDir.length()));
    new GopherItem(GopherType.DIR, description, dirName, host, PORT)
                 .write(writer);
    
    try {
      fileWriter = Files.newBufferedWriter(
                         Paths.get(dirName, GOPHERNAME),
                         StandardOpenOption.CREATE,
                         StandardOpenOption.TRUNCATE_EXISTING,
                         StandardOpenOption.WRITE,
                         StandardOpenOption.SYNC);
      files = Files.list(Paths.get(dirName))
                   .filter(p -> allSuffixes.contains(
                           p.toString().substring(p.toString().length() - 3)))
                   .sorted().collect(Collectors.toList());
                   // FIXME sort: some files start with numerics
      items = new ArrayList<>();  // addImageItem appends to this
      files.forEach(p -> addImageItem(p, items, fileMap));
      if (!items.isEmpty()) {
        // Go up the right number of directories. 
        // Note that this depends on dirName being formatted correctly!
        File f = new File(dirName);
        parents =  (int)dirName.substring(gopherDir.length()).chars()
                               .filter(ch -> ch == '/').count();
        for (int i = 0; i < parents; i++) {
          f = f.getParentFile();
        }                 
        selector = f.toString();
        logger.debug("parent selector: " + selector);
        new GopherItem(GopherType.DIR, "parent directory", selector,
                       host, PORT).write(fileWriter);
        if (readme != null) {
          selector = dirName + "/" + readme;
          new GopherItem(GopherType.TEXT, "README", selector, host, PORT)
             .write(fileWriter);
        }
        for (GopherItem item : items) {
          item.write(fileWriter);
        }
      }        
    } catch (Exception exc) {
      logger.error(exc.getMessage());
    } finally {
      fileWriter.close();
    }
    fileMap.clear();
  }
  
  /**
   * Add the binary-type entry for a disk image with an optional description.
   * @param  path The path to the disk image.
   * @param  items The list of gopher items to append to.
   * @param  fileMap A map of filenames to override descriptions.
   */
  private void addImageItem(Path path, java.util.List<GopherItem> items,
                            Map<String, String> fileMap) {
    String description, selector;
        logger.debug("path: " + path);
    // use filename as description by default
    description = path.getFileName().toString();
    if (fileMap.keySet().contains(description)) {
      // override if description provided
      description = (fileMap.get(description));
    }
    selector = path.toString();
    items.add(new GopherItem(GopherType.DIR, description, selector, 
                             host, PORT));
  }
  
  private void createGeoMetrixMap(Map<String, String> fileMap) {
    fileMap.put("Applications1A",      "GEOMET1.D64");
    fileMap.put("Applications1B",      "GEOMET2.D64");
    fileMap.put("Applications2A",      "GEOMET3.D64");
    fileMap.put("C= Program Files",    "GEOMET4.D64");
    fileMap.put("Application Data 1A", "GEOMET5.D64");
    fileMap.put("Application Data 1B", "GEOMET6.D64");
    fileMap.put("Application Data 2A", "GEOMET7.D64");
    fileMap.put("Application Data 2B", "GEOMET8.D64");
    fileMap.put("Photo Albums 1A",     "GEOMET9.D64");
    fileMap.put("Photo Albums 1B",     "GEOMET10.D64");
    fileMap.put("Photo Albums 2A",     "GEOMET11.D64");
    fileMap.put("Photo Albums 2B",     "GEOMET12.D64");
    fileMap.put("Photo Albums 3A",     "GEOMET13.D64");
    fileMap.put("BASIC Programs 1A",   "GEOMET14.D64");
    fileMap.put("Input/Printer Drivers 1A", "GEOMET15.D64");
    fileMap.put("Auto-Execs/DA's/System Files/128 Files/Documentation", 
                "GEOMET16.D64");
    fileMap.put("Documentation 1A",    "GEOMET17.D64");
    fileMap.put("Documentation 1B",    "GEOMET18.D64");
    fileMap.put("Supplementary 1A",    "GEOMET19.D64");
    fileMap.put("Supplementary 1B",    "GEOMET20.D64");
    fileMap.put("Supplementary 2A",    "GEOMET21.D64");
    fileMap.put("Supplementary 2B",    "GEOMET22.D64");
    fileMap.put("Supplementary 3A",    "GEOMET23.D64");
    fileMap.put("Supplementary 3B",    "GEOMET24.D64");
    fileMap.put("Supplementary 4A",    "GEOMET25.D64");
    fileMap.put("Supplementary 4B",    "GEOMET26.D64");          
  }
  public static void main(String[] argv) {
    Optional<Machine> machine = Optional.empty();
    
    if (argv.length == 0) {
      usage();
    }
    for (Machine thisMachine : Machine.values()) {
      if (argv[0].equals(thisMachine.toString())) {
        machine = Optional.of(thisMachine);
        break;
      }
    }
    if (machine.isPresent()) {
      target = machine.get();
      new CreateGeoSpecificGophermap();
    } else {
      logger.error("Unknown target: " + target);
      usage();
    }
  }
  
  private static void usage() {
    System.out.println("Usage: CreateGeoSpecificGophermap <target-machine>\n"
                     + "where <target-machine> is \"greyhand\", \"www\", "
                     + "\"orac\", or \"mafarka\".");
    System.exit(1);
  }
}
