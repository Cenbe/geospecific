module org.lyonlabs.geospecific {
  requires org.lyonlabs.d64;
  requires org.lyonlabs.geogopher;
  requires org.apache.commons.codec;
  requires org.apache.logging.log4j.core;
  requires org.apache.logging.log4j;
}
