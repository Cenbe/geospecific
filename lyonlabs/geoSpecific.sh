#!/bin/bash
if [ "$1" == "" ]; then
  JAVA="java "
elif [ "$1" == "debug" ]; then
  JAVA="java -agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=*:7070 "
fi
JAVADIR=/usr/local/java
$JAVA -p $JAVADIR:$JAVADIR/lyonlabs \
      -m org.lyonlabs.geospecific/org.lyonlabs.geospecific.CreateGeoSpecificGophermap \
      www
